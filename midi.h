
/*******************************************************************************/
/* Copyright (C) 2008 Jonathan Moore Liles                                     */
/*                                                                             */
/* This program is free software; you can redistribute it and/or modify it     */
/* under the terms of the GNU General Public License as published by the       */
/* Free Software Foundation; either version 2 of the License, or (at your      */
/* option) any later version.                                                  */
/*                                                                             */
/* This program is distributed in the hope that it will be useful, but WITHOUT */
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       */
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for   */
/* more details.                                                               */
/*                                                                             */
/* You should have received a copy of the GNU General Public License along     */
/* with This program; see the file COPYING.  If not,write to the Free Software */
/* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */
/*******************************************************************************/
#ifndef MIDI_H
#define MIDI_H

#include <jack/jack.h> //only for jack_nframes_t.
#include <string.h> // for memcpy and strcmp
#include <stdio.h>
#include <stdbool.h>
#include "common.h"
#include "list.h"
#include <malloc.h> // for testing linux kernel midi list
#include <stdlib.h> // for testing with rand()
/* define MIDI status bytes */

enum status_byte {
      STATUS_BIT       = 0x80,
      NOTE_OFF         = 0x80,
      NOTE_ON          = 0x90,
      AFTERTOUCH       = 0xA0,
      CONTROL_CHANGE   = 0xB0,
      PROGRAM_CHANGE   = 0xC0,
      CHANNEL_PRESSURE = 0xD0,
      PITCH_WHEEL      = 0xE0,
      CLEAR_CHAN_MASK  = 0xF0,
      MIDI_CLOCK       = 0xF8,
      SYSEX            = 0xF0,
      SYSEX_END        = 0xF7,
      META             = 0xFF
};

static inline int
midievent_size ( enum status_byte op )
{
   switch ( op )
   {
     case NOTE_ON:        case NOTE_OFF: case AFTERTOUCH:
     case CONTROL_CHANGE: case PITCH_WHEEL:
       return 3;
     case PROGRAM_CHANGE: case CHANNEL_PRESSURE:
       return 2;
     default:
       return 1;
   }
};

struct timestamp
{
  jack_nframes_t frame;
  jack_nframes_t period;
};


struct midievent
{
  struct list_head list;

  struct timestamp timestamp;

  struct {
    byte_t status,
           lsb,
           msb;
  } data;

  byte_t port;
};

void midievent_copy ( struct midievent *to, struct midievent *from );
void midievent_set_status ( struct midievent *me, byte_t status );
byte_t midievent_get_status ( struct midievent *me );
void midievent_set_channel ( struct midievent *me, byte_t channel );
byte_t midievent_get_channel ( struct midievent *me );
void midievent_set_opcode ( struct midievent *me, byte_t opcode );
byte_t midievent_get_opcode ( struct midievent *me );
void midievent_set_lsb ( struct midievent *me, byte_t n );
int midievent_get_lsb ( struct midievent *me );
void midievent_set_msb ( struct midievent *me, byte_t n );
int midievent_get_msb ( struct midievent *me );
bool midievent_is_note_on ( struct midievent *me );
bool midievent_is_note_off ( struct midievent *me );
void midievent_set_note ( struct midievent *me, char note );
unsigned char midievent_get_note ( struct midievent *me );
void midievent_set_pitch ( struct midievent *me, int n );
int midievent_get_pitch ( struct midievent *me);
void midievent_set_data ( struct midievent *me, byte_t D1, byte_t D2 );
void midievent_get_data ( struct midievent *me, byte_t *D1, byte_t *D2 );
void midievent_get_raw ( struct midievent *me, byte_t *p, int l);
int midievent_get_size ( struct midievent *me );
void midievent_set_note_velocity ( struct midievent *me, int vel );
unsigned char midievent_get_note_velocity ( struct midievent *me );
bool midievent_is_same_note ( struct midievent *me_lhs, struct midievent *me_rhs);
const char * midievent_name_from_opcode ( struct midievent *me );
int midievent_opcode_from_name ( const char *name );
void midievent_print ( struct midievent *me );
void midievent_pretty_print ( struct midievent *me );
int midievent_cmp ( struct list_head *a, struct list_head *b );
void print_event_list ( struct list_head *el );



#endif

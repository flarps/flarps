/************************************************************************/
/* Copyright (C) 2009 Lieven Moors                                      */
/*                                                                      */
/* This file is part of flarps.                                         */
/*                                                                      */
/* Flarps is free software: you can redistribute it and/or modify       */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* Flarps is distributed in the hope that it will be useful,            */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        */
/* GNU General Public License for more details.                         */
/*                                                                      */
/* You should have received a copy of the GNU General Public License    */
/* along with Flarps.  If not, see <http://www.gnu.org/licenses/>.      */
/************************************************************************/


/*------------------------------------------------------------------------------*/
/*  These functions are based on ideas from COMMON MUSIC, written by Rick Taube */
/*------------------------------------------------------------------------------*/


#include "rhythm.h"

using namespace std ;
using boost::rational ;


Token::Token(ratio r, string s, int d){
	rat = r ;
	sym = s ;
	dot = d ;
}

Dot::Dot(string s, ratio r){
	sym = s ;
	rat = r ;
}

void initialize_rhythm_map (){
	vector<Token> tokens ;
	vector<Dot> dots ;

	tokens.push_back( Token( ratio(1, 64), "x", 0 ) ) ;
	tokens.push_back( Token( ratio(1, 32), "t", 1 ) ) ;
	tokens.push_back( Token( ratio(1, 16), "s", 2 ) ) ;
	tokens.push_back( Token( ratio(1, 8), "e", 3 ) ) ;
	tokens.push_back( Token( ratio(1, 4), "q", 4 ) ) ;
	tokens.push_back( Token( ratio(1, 2), "h", 5 ) ) ;
	tokens.push_back( Token( ratio(1, 1), "w", 6 ) ) ;

	dots.push_back( Dot( ".", ratio(1, 2) ) ) ;
	dots.push_back( Dot( "..", ratio(3, 4) ) ) ;
	dots.push_back( Dot( "...", ratio(7, 8) ) ) ;
	dots.push_back( Dot( "....", ratio(15, 16) ) ) ;
	dots.push_back( Dot( ".....", ratio(31, 32) ) ) ;
	dots.push_back( Dot( "......", ratio(63, 64) ) ) ;

	for ( uint i = 0 ; i < tokens.size(); i++){
		ratio t_rat = tokens[i].rat ;
		string t_sym = tokens[i].sym ;
		int t_dot = tokens[i].dot ;
		ratio triple_ratio(2, 3) ; 

		RHYTHMS.insert( pair_sr( t_sym , t_rat ) ) ;
		RHYTHMS.insert( pair_sr( "t" + t_sym , t_rat * triple_ratio ) ) ;

		for (int i = 0 ; i < t_dot ; i++){
			string d_sym = dots[i].sym ;
			ratio d_rat = dots[i].rat ;

			RHYTHMS.insert( pair_sr( t_sym + d_sym, t_rat + (t_rat * d_rat)) ) ;
			RHYTHMS.insert( pair_sr( "t" + t_sym + d_sym, (t_rat * triple_ratio) + (t_rat * triple_ratio * d_rat)) ) ;
		}
	}
}

float rhythm ( string s, float tempo, float beat ){
	map<string,ratio>::iterator map_i ;
	map_i = RHYTHMS.find( s ) ;

	if ( map_i != RHYTHMS.end() ){
		ratio rat = map_i->second ;
		return (rational_cast<float>(rat) / beat) * (60 / tempo) ;
	}
	else {
		cout << '"' << s << '"' << " is not a valid rhythm string" << endl ;
                return false;
	}	
}

//int main(){}

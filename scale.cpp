/************************************************************************/
/* Copyright (C) 2009 Lieven Moors                                      */
/*                                                                      */
/* This file is part of flarps.                                         */
/*                                                                      */
/* Flarps is free software: you can redistribute it and/or modify       */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* Flarps is distributed in the hope that it will be useful,            */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        */
/* GNU General Public License for more details.                         */
/*                                                                      */
/* You should have received a copy of the GNU General Public License    */
/* along with Flarps.  If not, see <http://www.gnu.org/licenses/>.      */
/************************************************************************/

#include <iostream>
#include "scale.h"

using namespace std ;

// initialize the scale slot

void Scale::init_scale (){
	int tmp[7] = {2, 2, 1, 2, 2, 2, 1} ;
	for ( int i = 0; i < 7; i++ ){
		SCALE[i] = tmp[i] ;
	}
}

// the constructor

Scale::Scale () {
	init_scale() ;
	FLARPS = 0 ;
	BASENOTE = 60 ;
	INDEX = 0 ;
	MIN = 0 ;
	MAX = 127 ;
	MODE = 0 ;
	STEPS_SUM = 0 ;
	SIGN = 1 ;
}

// reorder the scale vector with the number at index first

void Scale::transpose_scale ( int index ){	
	int tmp[7] ;
	for (int i = 0; i < 7; i++){
		int j = (i + index) % 7 ;
		tmp[j] = SCALE[i] ;
	}
	for (int i = 0; i < 7; i++){
		SCALE[i] = tmp[i] ;
	}
}

// Calculate how much the basenote has to be increased for a number of flarps.
// At 2 sharps, we go up one, at 6 flats (-6) we go down one.  

int Scale::flarps_to_basenote_inc ( int flarps ){
	if ( flarps < 0 ){
		return (flarps - 1) / 7 ;
	}
	else {
		return (flarps + 5) / 7 ;
	}
}

// FLARPS may have contributed allready to a transposition of the basenote.
// That's why we calculate the contribution of newly added flarps as the difference
// between the contribution of the total sum of flarps (old and new), and the contribution
// of the old FLARPS

void Scale::transpose_basenote ( int flarps ){
	BASENOTE = BASENOTE + flarps_to_basenote_inc( FLARPS + flarps ) - flarps_to_basenote_inc( FLARPS ) ;
}

// calculate the index which we will use to reorder the scale vector.
// if we have negative flarps (flats), we take the complement within 7.
// equivalences: 6 sharps =~ 1 flat, 5 sharps =~ 2 flats etc. 

int Scale::flarps_to_index ( int flarps ){
	int i = flarps ;
	if ( i < 0 ){
		i = 7 + i ;
	}
	return ( 4 * i ) % 7 ;
}

// transpose: update the scale vector, the basenote, and FLARPS

void Scale::transpose ( int flarps ){
	int f = flarps % 7 ;
	transpose_scale ( flarps_to_index( f ) ) ;
	transpose_basenote ( flarps ) ;
	FLARPS = FLARPS + f ;
}

// calculate the size of a step in halfnotes
// we use the word stepsize, because step is also an object which has a size slot.

int Scale::stepsize_to_halfnotes ( int stepsize ){
	int step_oct = stepsize / 7 * 12 ;
	int step_mod = stepsize % 7 ;
	int index_old = INDEX ;
	int index_new = (INDEX + step_mod) % 7;
	int i1, i2 ;
	int sum = 0 ;

	// if new index is negative, we take it's complement within the seven indexes

	if ( index_new < 0 ){
		index_new = 7 + index_new ;
	}

	// we will add the steps from the smaller index until the greater,
 	// and correct afterwards 

	if ( index_new < index_old ) {
		i1 = index_new ; i2 = index_old ;
	}
	else { 
		i1 = index_old ; i2 = index_new ; 
	}

	// now we add the steps

	for (int i = i1; i < i2; i++ ){
		sum += SCALE[i];
	}

	// now we correct for different orderings of index_new and index_old
	// in these cases we take the complement within the octave

	if ( ((step_mod < 0) && (index_new > index_old)) ||
			 ((step_mod > 0) && (index_new < index_old)) ){	
		sum = 12 - sum ;
	}

  // now we add or substract the octaves

	if ( step_mod < 0 ){
		return step_oct - sum ;
	}
	else {
		return step_oct + sum ;
	}
}


int abs( int n ){
	if (n < 0) return -n; else return n ;
}

/*-------------------------------------------------------------------------*/
/* This function has been adapted from COMMON MUSIC, written by Rick Taube */
/*-------------------------------------------------------------------------*/

int Scale::fit( int num )
{
	if ( MIN > MAX ){
		int x = MIN ;
		MIN = MAX ;
		MAX = x ;
	}
	if ( (MIN <= num) && (num <= MAX) ){
		return num ;
	}
	else {
		int bound = ( num > MAX ) ? MAX : MIN ;
		int range = MAX - MIN ;

		switch ( MODE ){
			case 0: {
				int base = (bound == MAX) ? MIN : MAX ;
				return base + ((num - bound) % range) ;
				break ;
			}

			case 1: {
				int r = range * 2 ;
				int v = (num - bound) % r ;
				int x = v >= 0 ? v - r : v + r ;
				int y = abs(v) > range ? x : -v ;
				return bound + y ;
				break ;
			}
                        default: {
				cerr << "Invalid Number for mode! " << MODE << endl;
				return num;
                        }
		}
	}
}


// hfb stands for halfnotes from basenote.
// Calculate the size of step in halfnotes with the basenote as reference point.
// if we have a negative step, "+" will do the right thing (negative step becomes smaller).

int Scale::stepsize_to_hfb ( int stepsize ){
	int hfb_till_index = 0 ;

	for (int i = 0; i < INDEX; i++){
		hfb_till_index += SCALE[i];
	}

	return stepsize_to_halfnotes(stepsize) + hfb_till_index ;
}

// Calculate how much we have to increment the basenote based on the stepsize (in halfnotes) relative to basenote
// This is equal to the number of octave boundaries that are crossed times twelve.
// If hfb is negative, we allready cross one octave boundary (-12). 

int Scale::hfb_to_basenote_inc( int hfb ){
	int x = 0 ;
	if ( hfb < 0 ){
		x = (hfb + 1) / 12 ;	// if hfb is -12, we haven't crossed extra octave yet, if -13 we did. 
		return (x - 1) * 12 ;	// but we always add extra octave for negative step
	}
	else {
		x = hfb / 12 ;
		return x * 12 ;
	}
}

// Here we calculate the new index after taking a step.
// The % operator returns negative values for negative input (remainder, not modulo), so we have to correct for that.

int Scale::stepsize_to_index_inc( int stepsize ){
	int mod_size = (INDEX + stepsize) % 7 ; 
	if ( mod_size < 0 ){
		return 7 + mod_size ;
	}
	else {
		return mod_size ;
	}
}

// take a step in scale: calculate note and update INDEX and BASENOTE

// return the asked for step_size when within bounds, otherwise return the difference between
// the new position within step range after applying fit on step, and the position before applying step. 

int Scale::take_step ( int stepsize )
{
	int s = STEPS_SUM + (stepsize * SIGN) ;
	int step ;

	if( (s >= MIN) && (s <= MAX) ){
		step = (stepsize * SIGN) ;
	}
	else {
		step = fit( s ) - STEPS_SUM ;

		if( MODE == 1 ) SIGN = -SIGN ;
		//cout << step << endl ;
	}

	STEPS_SUM += step ;
	
	int hfb = stepsize_to_hfb( step ) ;
	int note = BASENOTE + hfb ;
	INDEX = stepsize_to_index_inc( step ) ;
	BASENOTE += hfb_to_basenote_inc( hfb ) ;
	return note ;
}

int Scale::get_step ()
{
  int hfb_till_index = 0;

  for (int i = 0; i < INDEX; i++){
    hfb_till_index += SCALE[i];
  }
  return BASENOTE + hfb_till_index;
}

void Scale::debug(){
	cout << "BASENOTE = " << BASENOTE << endl ;
	cout << "INDEX = " << INDEX << endl ;
	cout << "FLARPS = " << FLARPS << endl ;
}

/*
int main()
{
  Scale s;
  cout << "s.get_step:" << s.get_step() << endl;
  cout << "s.take_step:" << s.take_step(13) << endl;
  cout << "s.get_step:" << s.get_step() << endl;
}
*/

/*
int main (){
	Scale s;
	int step = 1 ;

	for ( int x = 0; x < 130; x++ ){
		//s.debug( step );
		cout << "note is: " << s.take_step( step ) << " with step: " << step << endl ;
	}

	//s.transpose( 2 ) ;
	//step = -1 ;

	//for ( int x = 0; x < 10; x++ ){
		//s.debug( step );
	//	cout << "note is: " << s.take_step( step ) << " with step: " << step << endl ;
	//}
	//cout << "flarps = " << FLARPS << endl << "basenote = " << BASENOTE << endl ;
	//transpose( -8 );
	//cout << "flarps = " << FLARPS << endl << "basenote = " << BASENOTE << endl ;
}
*/

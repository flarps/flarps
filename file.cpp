/************************************************************************/
/* Copyright (C) 2009 Lieven Moors                                      */
/*                                                                      */
/* This file is part of flarps.                                         */
/*                                                                      */
/* Flarps is free software: you can redistribute it and/or modify       */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* Flarps is distributed in the hope that it will be useful,            */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        */
/* GNU General Public License for more details.                         */
/*                                                                      */
/* You should have received a copy of the GNU General Public License    */
/* along with Flarps.  If not, see <http://www.gnu.org/licenses/>.      */
/************************************************************************/

#include "file.h"

using namespace std ;
using boost::rational ;

// three maps which will be used to look up if a string from the file is valid (first field),
// and to associate the right predicate with that string.

map< string,bool (*)( const vec_str&, uint )> GLOBAL_PROPERTIES ;
map< string,bool (*)( const vec_str&, uint )> SCALE_PROPERTIES ;
map< string,bool (*)( const vec_str&, uint )> STEP_PROPERTIES ;


// read file line by line and put every line in vector

void read_file ( const string& filename, vector<string>* buf )
{ 
  string tmp;
  ifstream ifs( filename.c_str() );

  if( !ifs ){
	throw file_error( filename ) ;
  }
  while( getline( ifs, tmp ) ){
     buf->push_back( tmp );
  }
  if ( !ifs.eof() ){
	throw file_error( filename ) ;
  }
}

// split a string in words and put them in vector, ignore comments

void tokenize_string(const string& str, vector<string>& tokens)
{
  const string delimiters = " \t" ;
  string::size_type pos_a = str.find_first_not_of(delimiters, 0) ;
  string::size_type pos_b = str.find_first_of(delimiters, pos_a) ;

  while (pos_b != string::npos || pos_a != string::npos)
  {
    string token = str.substr(pos_a, pos_b - pos_a) ;
    if ( token.substr(0 , 1) == "#" ){  
      break ;
    }
    tokens.push_back( token ) ;
    pos_a = str.find_first_not_of(delimiters, pos_b) ;
    pos_b = str.find_first_of(delimiters, pos_a) ;
  }
}

// put all the words of file in a two dimensional vector. 
// first index accesses a line, second a word of that line.
// note: don't push empty lines

void tokenize_file(vector<string>& lines, vec_str_2& tokens)
{
  for(uint i = 0; i < lines.size(); i++){
    vector<string> v ;
    tokenize_string( lines[i], v ) ;
    if ( v.size() > 0 ){
      tokens.push_back( v ) ;
    }
  }
}

// template to check if a string can be cast to a certain type
// stores the value into a variable of that type.

template <class T>
bool type_from_string( T& t, const string& s )
{
  istringstream iss(s) ;
  return !(iss >> t).fail() ;
}

// variant which has the cast value as return value

template <class T>
T get_from_string( const string& s)
{
  istringstream iss(s) ;
  T t ;
  iss >> t ;
  return t ;
}

// lots of predicates which will be used to check the "words" in a file.

bool is_rhythm( const vec_str& line, uint index )
{
  map<string,ratio>::iterator map_i ;
  map_i = RHYTHMS.find( line[index] ) ;
  if ( map_i == RHYTHMS.end() ){
    return false ;
  }
  else return true ;
}

bool is_int( const vec_str& line, uint index )
{
  int i ;
  if ( type_from_string<int>( i, line[index] )){
    return true ;
  }
  else return false ;
}

bool is_pos_int( const vec_str& line, uint index ) // TODO: check -> cannot use uint as type, but seems to work with get_from_string!
{
  int i ;
  bool is_int = type_from_string<int>( i, line[index] ) ;
  if ( is_int && (i >= 0) ){
    return true ;
  }
  else return false ;
}

bool is_pos_float( const vec_str& line, uint index )
{
  float f ;
  bool is_float = type_from_string<float>( f, line[index] ) ;
  if ( is_float && (f >= 0) ){
    return true ;
  }
  else return false ;
}

bool is_midi_int( const vec_str& line, uint index )
{
  int i ;
  bool is_int = type_from_string<int>( i, line[index] ) ;
  if ( is_int && i >= 0 && i < 128 ){
    return true ;
  }
  else return false ;
}

bool is_index_int( const vec_str& line, uint index )
{
  int i ;
  bool is_int = type_from_string<int>( i, line[index] ) ;
  if ( is_int && i >= 0 && i < 7 ){
    return true ;
  }
  else return false ;
}

bool is_octave_int( const vec_str& line, uint index )
{
  int i ;
  bool is_int = type_from_string<int>( i, line[index] ) ;
  if ( is_int && i >= 0 && i <= 10 ){
    return true ;
  }
  else return false ;
}


bool is_channel_int( const vec_str& line, uint index )
{
  int i ;
  bool is_int = type_from_string<int>( i, line[index] ) ;
  if ( is_int && i >= 0 && i <= 15 ){
    return true ;
  }
  else return false ;
}


bool is_height_line( const vec_str& line, uint index )
{
  bool b = true ;
  for( uint i = index; i < line.size(); i++ )
  {
    b = b && is_int( line, i ) ;
    if( b == false ){
      return b ;
    }
  }
  return b ;
}

bool is_rhythm_line( const vec_str& line, uint index )
{
  bool b = true ;
  for( uint i = index; i < line.size(); i++ )
  {
    b = b && is_rhythm( line, i ) ;
    if( b == false ){
      return b ;
    }
  }
  return b ;
}

bool is_velocity_line( const vec_str& line, uint index )
{
  bool b = true ;
  for( uint i = index; i < line.size(); i++ )
  {
    b = b && is_midi_int( line, i ) ;
    if( b == false ){
      return b ;
    }
  }
  return b ;
}

// to intialize the three maps

void init_arp_properties()
{
  GLOBAL_PROPERTIES.clear() ;
  SCALE_PROPERTIES.clear() ;
  STEP_PROPERTIES.clear() ;

  GLOBAL_PROPERTIES.insert( pair_sf( "tempo", is_pos_float ) ) ; // GLOBAL_PROPERTIES apply to all the arps
  GLOBAL_PROPERTIES.insert( pair_sf( "beat", is_pos_float ) ) ;
  GLOBAL_PROPERTIES.insert( pair_sf( "sync", is_rhythm ) ) ;

  SCALE_PROPERTIES.insert( pair_sf( "flarps", is_int ) ) ; //TODO: change the name to ARP_PROPERTIES
  SCALE_PROPERTIES.insert( pair_sf( "index", is_index_int ) ) ;
  SCALE_PROPERTIES.insert( pair_sf( "octave", is_octave_int ) ) ;
  SCALE_PROPERTIES.insert( pair_sf( "min", is_int ) ) ;
  SCALE_PROPERTIES.insert( pair_sf( "max", is_int ) ) ;
  SCALE_PROPERTIES.insert( pair_sf( "mode", is_pos_int ) ) ;
  SCALE_PROPERTIES.insert( pair_sf( "channel", is_channel_int ) ) ;

  STEP_PROPERTIES.insert( pair_sf( "height", is_height_line ) ) ; // STEP_PROPERTIES are special because they can have different lengths
  STEP_PROPERTIES.insert( pair_sf( "width", is_rhythm_line ) ) ;
  STEP_PROPERTIES.insert( pair_sf( "duration", is_rhythm_line ) ) ;
  STEP_PROPERTIES.insert( pair_sf( "velocity", is_velocity_line ) ) ;
}


bool is_arp_property_value( const vec_str& line, uint index, pair_sf* pair )
{
  return pair->second( line, index ) ;
}


bool is_arp_property( const vec_str& line, uint index, ushort m, pair_sf* p )
{
  property_map* MAP = &GLOBAL_PROPERTIES ;
  property_map_i map_i ;

  switch ( m )
  {
    case 0:
    break ;
    case 1: {
      MAP = &SCALE_PROPERTIES ;
      break ;
    }
    case 2: {
      MAP = &STEP_PROPERTIES ;
      break ;
    }
    default:
      return false ;
  }
  map_i = MAP->find( line[index] ) ;
  bool b = ( map_i != MAP->end() ) ;

  if (b){ 
    *p = *map_i ;
  }
  return ( b ) ;
}

// check all the token of the file

void check_tokens ( const vec_str_2& tokens )
{
  init_arp_properties() ;
  pair_sf p("", is_int) ;

  for ( uint i = 0; i < tokens.size(); i++ )
  {
    if ( tokens[i].size() == 2 )
    {
      if ( is_arp_property( tokens[i], 0, 0, &p ) )
      {
        if ( !is_arp_property_value( tokens[i], 1, &p ) )
        {
          throw syntax_error( "value '" + tokens[i][1] + "' not valid for: " + tokens[i][0] ) ;
        }
      }
      else throw syntax_error( "'" + tokens[i][0] + "' is not a global variable" ) ;
    }
    else if ( tokens[i].size() >= 3 )
    {
      if ( is_pos_int ( tokens[i], 0 ) )
      {
        if ( is_arp_property( tokens[i], 1, 1, &p ) )
        {
          if ( !is_arp_property_value( tokens[i], 2, &p ) )
          {
            throw syntax_error( "value '" + tokens[i][2] + "' not valid for: " + tokens[i][0] + " " + tokens[i][1] ) ;
          }
        }
        else if ( is_arp_property( tokens[i], 1, 2, &p ) )
        {
          if ( !is_arp_property_value( tokens[i], 2, &p ) )
          { 
            throw syntax_error( "one of the values is not valid for: " + tokens[i][0] + " " + tokens[i][1] ) ;
          }
        }
        else throw syntax_error( "'" + tokens[i][1] + "' is not a valid arpeggio variable" ) ;
      }
      else throw syntax_error( "'" + tokens[i][0] + "' is not a valid index for an arpeggio" ) ;
    }
    else throw syntax_error( "the line that starts with '" + tokens[i][0] + "' should have two or more fields" ) ;
  }	
}

// sort the tokens, so that the global variables come first, and that lines starting with int are sorted

bool sort_arp_lines ( const vec_str& vec1, const vec_str& vec2 ){
  uint x, y ;
  if ( type_from_string<uint>( x, vec1[0]) ) // if first starts with int
  {
    if ( type_from_string<uint>( y, vec2[0]) ) // if first and second start with int
    {
      return y >= x ; // sort according to number
    }
    else return false ; //  first starts with int, second not
  }
  else return true ; // if first doesn't start with int
}


uint arp_properties_max_size( vec_str_2& tokens, uint index )
{
  pair_sf p ;
  uint max_size = 0 ;
  bool got_index ;
  uint cur_index, prev_index ;

  type_from_string<uint>( cur_index, tokens[index][0] ) ;	
  prev_index = cur_index ;


  for (uint i = index; i < tokens.size(); i++)
  {
    got_index = type_from_string<uint>( cur_index, tokens[i][0] ) ;

    if ( got_index )
    {	
      if( cur_index == prev_index )
      {
        if ( is_arp_property( tokens[i], 1, 2, &p ) )
        {
          if(tokens[i].size() > max_size)
            max_size = tokens[i].size();
        }
        prev_index = cur_index ; 
      }
      else return max_size ;
    }
    else return max_size ;
  }
  return max_size ;
}

// I'm using stable_sort, because sort has a bug that causes a segfault,
// when I define more than three arps in the text file.
// Gdb shows that this happens when basic_istringstream constructor is called
// in the get_from_string template.
// I tried to fix this bug, by removing the template, and by using strtol.
// Sort segfaulted just the same, when string.c_str() was called.
// Know I know that a stable_sort is just a sort that doesn't crash :-)

void load_tokens( vec_str_2& tokens, vector<Arp>* arps )
{
  Scale* scale;
  vector<Step>* steps;
  int prev_index, cur_index ;
  uint max_size = 0 ;
  pair_sf p ;
  int midi_channel = -1 ;

  stable_sort( tokens.begin(), tokens.end(), sort_arp_lines ) ;

  prev_index = -1 ;


  for ( uint i = 0; i < tokens.size(); i++ )
  {
    if( is_arp_property( tokens[i], 0, 0, &p ) ) // globals have to be loaded first (see sort) because they are used by set_width and set_duration functions
    {
      if( p.first.compare( "tempo" ) == 0 ){
        float f = get_from_string<float>( tokens[i][1] ) ;
        TEMPO = f ;
      }
      if( p.first.compare( "beat" ) == 0 ){
        float f = get_from_string<float>( tokens[i][1] ) ;
        BEAT = f ;
      }
      if( p.first.compare( "sync" ) == 0 ){
        SYNC = rhythm(tokens[i][1]);
      }
    }
    else if ( type_from_string<int>( cur_index, tokens[i][0] ) )
    {
      if (prev_index != cur_index)
      {
        if(prev_index != -1)
        {
          Arp arp( *scale, *steps ) ;

          if( midi_channel > -1 )  // we set channel here, because arp is constructed only now (after scale and steps)
          {
            arp.channel = midi_channel ; //TODO: we could set channel always, and not rely on default parameter values of Arp constructor
            midi_channel = -1 ;
          }
 
          arps->push_back( arp ) ;
          delete scale; delete steps;
        } 
        scale = new Scale();
        max_size = (arp_properties_max_size( tokens, i ) - 2) ;
        steps = new vector<Step>(max_size);
      }

      if ( is_arp_property( tokens[i], 1, 2, &p ) )
      {
        int jmod ;

        if ( p.first.compare( "height" ) == 0 )
        {
          for ( uint j = 0; j < max_size; j++ )
          {
            jmod = (j % (tokens[i].size() - 2)) + 2 ;
            int n = get_from_string<int>( tokens[i][jmod] ) ;
            (*steps)[j].set_height( n ) ;	
          }
        }
        if ( p.first.compare( "width" ) == 0 )
        {
          for ( uint j = 0; j < max_size; j++ )
          {
            jmod = (j % (tokens[i].size() - 2)) + 2 ;
            string s = get_from_string<string>( tokens[i][jmod] ) ; //TODO: can't we just take the string directly?
            (*steps)[j].set_width( s ) ;	
          }
        }
        if ( p.first.compare( "duration" ) == 0 )
        {
          for ( uint j = 0; j < max_size; j++ )
          {
            jmod = (j % (tokens[i].size() - 2)) + 2 ;
            string s = get_from_string<string>( tokens[i][jmod] ) ;
            (*steps)[j].set_duration( s ) ;	
          }
        }
        if ( p.first.compare( "velocity" ) == 0 )
        {
          for ( uint j = 0; j < max_size; j++ )
          {
            jmod = (j % (tokens[i].size() - 2)) + 2 ;
            int n = get_from_string<int>( tokens[i][jmod] ) ;
            (*steps)[j].set_velocity( n ) ;	
          }
        }
      }
      else if ( is_arp_property( tokens[i], 1, 1, &p ) )
      {
        int n = get_from_string<int>( tokens[i][2] ) ;

        if ( p.first.compare( "flarps" ) == 0 ){
          (*scale).set_flarps( n ) ;
        }
        if ( p.first.compare( "index" ) == 0 ){
          (*scale).set_index( n ) ;
        }
        if ( p.first.compare( "octave" ) == 0 ){
          (*scale).set_octave( n ) ;
        }
        if ( p.first.compare( "mode" ) == 0 ){
          (*scale).set_mode( n ) ;
        }
        if ( p.first.compare( "min" ) == 0 ){
          (*scale).set_min( n ) ;
        }
        if ( p.first.compare( "max" ) == 0 ){
          (*scale).set_max( n ) ;
        }
        if ( p.first.compare( "channel" ) == 0 ){
          midi_channel = n;
        }
      }
      prev_index = cur_index ;
    }
  }

  Arp arp( *scale, *steps ) ; // make the final arp

  if( midi_channel > -1 )
  {
    arp.channel = midi_channel ;
    midi_channel = -1 ;
  }
 
  arps->push_back( arp ) ;
  delete scale; delete steps;

}



void read_load_file (vector<Arp>* arps) // TODO: change name to load_file()
{
  vector<string> lines ;
  vec_str_2 tokens ;

  try {
    read_file( filename, &lines );
  }
  catch (file_error& x){
    cout << "cannot read input file: " << x.what() << endl;
    exit(1) ;
  }

  expand_repeats( lines );
  tokenize_file( lines, tokens ) ;

  try {
    check_tokens( tokens ) ;
  }
  catch (syntax_error& x){
    throw(x);
  }

  load_tokens( tokens, arps ) ;
  //print_arps( arps );
}



string expand_repeats_line( string str )
{
  string new_string = "";

  int status;
  const char* cstring = str.c_str();
  char pattern[]="[(]([^(]+)[)]([[:digit:]]+)"; // subexpressions are between parenthesis :-)

  // two subexpressions: first is everything between parenthesis, second is the repeat number
  // pmatch[i][0] -> whole match; pmatch[i][1] -> first subexpression; pmatch[i][2] -> second subexpression.
  // There is a arbitrary maximum of 100 possible repeat expansions per line.

  regmatch_t pmatch[99][2];
  regex_t reg;

  regcomp(&reg, pattern, REG_EXTENDED);

  uint offset = 0;

  for(uint i = 0; i < 100; i++)
  {
    status=regexec(&reg, cstring + offset, 3, pmatch[i], 0); // look for the next match

    if (status==0)
    { 
      // printf("match\n");
      new_string.append(cstring + offset, (pmatch[i][0].rm_so)); // append all characters from offset upto match

      uint repeat_index = pmatch[i][2].rm_so + offset; // get index in string for first char indicating the repeat value
      const char* repeat_p = &cstring[repeat_index]; // get a pointer to it
      uint repeat = atoi(repeat_p); // convert the pointer to an int (atoi should check for decimals only)

      for(uint j = 0; j < repeat; j++)
      {
        uint chars_index = pmatch[i][1].rm_so + offset; // get index of chars that should be repeated
        const char* chars_p = &cstring[chars_index]; // get a pointer to those chars
        uint n_chars = pmatch[i][1].rm_eo - pmatch[i][1].rm_so; // how many chars should be repeated
        new_string.append(" "); // add extra space at the beginning
        new_string.append(chars_p, n_chars); // append chars
      }
      // printf("repeat = %i\n", repeat);
    }
    else 
    {
      // printf("no match\n"); 
      regfree(&reg);

      if(i == 0) return str;
      else 
      {
        new_string.append(cstring + offset); // if no match is found, append everything untill end (NULL)
        // printf("%s\n", new_string.c_str());
        return new_string; // if no match is found, append everything untill end (NULL)
      }
    }

    offset = offset + pmatch[i][0].rm_eo; // set offset to the end of pattern
  }
  return str; // to stop compiler complaints
}


void expand_repeats(vector<string>& lines)
{
  for(uint i = 0; i < lines.size(); i++)
  {
    lines[i] = expand_repeats_line(lines[i]);
  }
}

void print_arps( vector<Arp>* arps )
{
  for ( uint x = 0; x < arps->size(); x++ )
  {
    cout << "new arp\n" << endl ;

    for ( uint y = 0; y < (*arps)[x].steps.size(); y++ )
    {
      cout << "step " << y << ": " ;
      cout << (*arps)[x].steps[y].get_height() << " " ;
      cout << (*arps)[x].steps[y].get_width() << " " ;
      cout << (*arps)[x].steps[y].get_duration() << " " ;
      cout << (*arps)[x].steps[y].get_velocity() << endl ;
    }
    cout << "\n" << "scale " << x << "\n" ;
    cout << "flarps = " << (*arps)[x].scale.get_flarps() << endl ;
    cout << "index = " << (*arps)[x].scale.get_index() << endl ;
    cout << "octave = " << (*arps)[x].scale.get_octave() << endl ;
    cout << "mode = " << (*arps)[x].scale.get_mode() << endl ;
    cout << "min = " << (*arps)[x].scale.get_min() << endl ;
    cout << "max = " << (*arps)[x].scale.get_max() << "\n" << endl ;
    cout << "channel = " << static_cast<uint>((*arps)[x].channel) << "\n" << endl ;
  }
  cout << "TEMPO = " << TEMPO << endl ;
  cout << "BEAT = " << BEAT << endl ;
}








// TODO make an object with: a name (string), a type (basic types), a pointer to predicate, and pointer to accessor
// make a vector of these objects instead of using a map.
// or better: name, a type (my own specific type), no predicate (let the type be restrictive), and accessor

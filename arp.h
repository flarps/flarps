/************************************************************************/
/* Copyright (C) 2009 Lieven Moors                                      */
/*                                                                      */
/* This file is part of flarps.                                         */
/*                                                                      */
/* Flarps is free software: you can redistribute it and/or modify       */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* Flarps is distributed in the hope that it will be useful,            */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        */
/* GNU General Public License for more details.                         */
/*                                                                      */
/* You should have received a copy of the GNU General Public License    */
/* along with Flarps.  If not, see <http://www.gnu.org/licenses/>.      */
/************************************************************************/

#ifndef ARP_H
#define ARP_H

#include <set>
#include <vector>
#include "common.h"
#include "step.h"
#include "scale.h"
#include "midi.h"

using std::vector ;

class Arp {

  public:
    Scale scale ;
    vector<Step> steps ;
    byte_t channel ;
    byte_t index_steps ;

    Arp( Scale, vector<Step> ) ;
    ~Arp() ;
} ;

#endif

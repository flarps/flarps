/************************************************************************/
/* Copyright (C) 2009 Lieven Moors                                      */
/*                                                                      */
/* This file is part of flarps.                                         */
/*                                                                      */
/* Flarps is free software: you can redistribute it and/or modify       */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* Flarps is distributed in the hope that it will be useful,            */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        */
/* GNU General Public License for more details.                         */
/*                                                                      */
/* You should have received a copy of the GNU General Public License    */
/* along with Flarps.  If not, see <http://www.gnu.org/licenses/>.      */
/************************************************************************/

/*-----------------------------------------------------------------------------*/
/* These functions are based on ideas from COMMON MUSIC, written by Rick Taube */
/*-----------------------------------------------------------------------------*/


#ifndef RHYTHM_H
#define RHYTHM_H

#include "common.h"
#include <map>
#include <iostream>
#include <vector>
#include <string>
#include <boost/rational.hpp>

using boost::rational ;
using boost::rational_cast ;
using namespace std ;

typedef rational<int> ratio ;
typedef pair<string,ratio> pair_sr ;


extern map<string,ratio> RHYTHMS ;
extern float TEMPO ;
extern float BEAT ;


class Token {
	public:
	ratio rat ;
	string sym ;
	int dot ;
	Token(ratio r, string s, int d);
} ;

class Dot {
	public:
	string sym ;
	ratio rat ;
	Dot(string s, ratio r) ;
} ;


void initialize_rhythm_map( void ) ;
float rhythm ( string s, float tempo = TEMPO, float beat = BEAT ) ;

#endif

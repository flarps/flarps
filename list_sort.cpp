// From: Mark J Roberts (mjr@znex.org)

// http://lkml.indiana.edu/hypermail/linux/kernel/0206.3/0576.html

/* A few months ago I adapted a merge sort implementation from
http://www.chiark.greenend.org.uk/~sgtatham/algorithms/listsort.html
to work with Linux's list.h. I figure I should get it into the
archives just in case anyone might want such a thing. So here it is.*/

#include "list.h"

// this makes it compileable by the g++ compiler.
// in C NULL would be defined as (void*)0, and this can be implicitly cast to other pointers.
// C++ requires explicit cast.

#define NULL (struct list_head *)0


void list_sort(struct list_head *head, int (*cmp)(struct list_head *a, struct list_head *b))
{
        struct list_head *p, *q, *e, *list, *tail, *oldhead;
        int insize, nmerges, psize, qsize, i;

        list = head->next;
        list_del(head);
        insize = 1;
        for (;;) {
                p = oldhead = list;
                list = tail = NULL;
                nmerges = 0;

                while (p) {
                        nmerges++;
                        q = p;
                        psize = 0;
                        for (i = 0; i < insize; i++) {
                                psize++;
                                q = q->next == oldhead ? NULL : q->next;
                                if (!q)
                                        break;
                        }

                        qsize = insize;
                        while (psize > 0 || (qsize > 0 && q)) {
                                if (!psize) {
                                        e = q;
                                        q = q->next;
                                        qsize--;
                                        if (q == oldhead)
                                                q = NULL;
                                } else if (!qsize || !q) {
                                        e = p;
                                        p = p->next;
                                        psize--;
                                        if (p == oldhead)
                                                p = NULL;
                                } else if (cmp(p, q) <= 0) {
                                        e = p;
                                        p = p->next;
                                        psize--;
                                        if (p == oldhead)
                                                p = NULL;
                                } else {
                                        e = q;
                                        q = q->next;
                                        qsize--;
                                        if (q == oldhead)
                                                q = NULL;
                                }
                                if (tail)
                                        tail->next = e;
                                else
                                        list = e;
                                e->prev = tail;
                                tail = e;
                        }
                        p = q;
                }

                tail->next = list;
                list->prev = tail;

                if (nmerges <= 1)
                        break;

                insize *= 2;
        }

        head->next = list;
        head->prev = list->prev;
        list->prev->next = head;
        list->prev = head;
}

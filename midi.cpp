/*******************************************************************************/
/* This file was modified by Lieven Moors                                      */
/* It was taken from the Non-sequencer project                                 */
/*******************************************************************************/
/* Copyright (C) 2008 Jonathan Moore Liles                                     */
/*                                                                             */
/* This program is free software; you can redistribute it and/or modify it     */
/* under the terms of the GNU General Public License as published by the       */
/* Free Software Foundation; either version 2 of the License, or (at your      */
/* option) any later version.                                                  */
/*                                                                             */
/* This program is distributed in the hope that it will be useful, but WITHOUT */
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       */
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for   */
/* more details.                                                               */
/*                                                                             */
/* You should have received a copy of the GNU General Public License along     */
/* with This program; see the file COPYING.  If not,write to the Free Software */
/* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */
/*******************************************************************************/

#include "midi.h"

static const char *opcode_names[] =
{
    "Note Off",
    "Note On",
    "Aftertouch",
    "Control Change",
    "Program Change",
    "Channel Pressure",
    "Pitch Wheel"
};

void
midievent_copy ( struct midievent *to, struct midievent *from )
{
  to->timestamp = from->timestamp;
  to->data = from->data;
  to->port = from->port;
}

void
midievent_set_status ( struct midievent *me, byte_t status )
{
    (*me).data.status = status;
}

byte_t
midievent_get_status ( struct midievent *me ) 
{
    return (*me).data.status;
}

void
midievent_set_channel ( struct midievent *me, byte_t channel )
{
    (*me).data.status = ((*me).data.status & 0xF0) | (channel & 0x0F);
}

byte_t
midievent_get_channel ( struct midievent *me ) 
{
    return (*me).data.status & 0x0F;
}

void
midievent_set_opcode ( struct midievent *me, byte_t opcode )
{
    (*me).data.status = ((*me).data.status & 0x0F) | (opcode & 0xF0);
}

byte_t
midievent_get_opcode ( struct midievent *me ) 
{
    return (*me).data.status & 0xF0;
}

void
midievent_set_lsb ( struct midievent *me, byte_t n )
{
    (*me).data.lsb = n & 0x7F;
}

int
midievent_get_lsb ( struct midievent *me ) 
{
    return (*me).data.lsb;
}

void
midievent_set_msb ( struct midievent *me, byte_t n )
{
    (*me).data.msb = n & 0x7F;
}

int
midievent_get_msb ( struct midievent *me ) 
{
    return (*me).data.msb;
}

bool
midievent_is_note_on ( struct midievent *me ) 
{
    return (midievent_get_opcode(me) == NOTE_ON);
}

bool
midievent_is_note_off ( struct midievent *me ) 
{
    return (midievent_get_opcode(me) == NOTE_OFF);
}

void
midievent_set_note ( struct midievent *me, char note )
{
    (*me).data.lsb = note & 0x7F;
}

unsigned char
midievent_get_note ( struct midievent *me ) 
{
    return (*me).data.lsb;
}

void
midievent_set_pitch ( struct midievent *me, int n )
{
    n += 0x2000;

    (*me).data.lsb = n & 0x7F;
    (*me).data.msb = (n >> 7) & 0x7F;
}

int
midievent_get_pitch ( struct midievent *me) 
{
    return (((*me).data.msb << 7) | (*me).data.lsb) - 0x2000;
}

void
midievent_set_data ( struct midievent *me, byte_t D1, byte_t D2 )
{
    (*me).data.lsb = D1 & 0x7F;
    (*me).data.msb = D2 & 0x7F;
}

void
midievent_get_data ( struct midievent *me, byte_t *D1, byte_t *D2 ) 
{
    *D1 = (*me).data.lsb;
    *D2 = (*me).data.msb;
}

void
midievent_get_raw ( struct midievent *me, byte_t *p, int l) 
{
    memcpy( p, &(*me).data, l );
}

int
midievent_get_size ( struct midievent *me ) 
{
    return midievent_size( (enum status_byte)midievent_get_opcode(me) );
}

void
midievent_set_note_velocity ( struct midievent *me, int vel )
{
    (*me).data.msb = vel & 0x7F;
}


unsigned char
midievent_get_note_velocity ( struct midievent *me ) 
{
    return (*me).data.msb;
}

bool
midievent_is_same_note ( struct midievent *me_lhs, struct midievent *me_rhs) 
{
    return midievent_get_channel(me_lhs) == midievent_get_channel(me_rhs) && 
             midievent_get_note(me_lhs) == midievent_get_note(me_rhs);
}

/** get name from opcode */
const char *
midievent_name_from_opcode ( struct midievent *me ) 
{
    return opcode_names[ (midievent_get_opcode(me) >> 4) - 8 ];
}

/** get opcode from name */
int
midievent_opcode_from_name ( const char *name ) 
{
    unsigned int i;
    for ( i = elementsof( opcode_names ); i--; )
        if ( ! strcmp( name, opcode_names[ i ] ) )
            return (i + 8) << 4;

    return -1;
}

/** print event in hexadecimal */
void
midievent_print ( struct midievent *me ) 
{
    printf( "[%06d] [%06d] %02X %02X %02X\n",
            (*me).timestamp.period,
            (*me).timestamp.frame,
            (*me).data.status,
            (*me).data.lsb,
            (*me).data.msb );
}

/** print event in english/decimal */
void
midievent_pretty_print ( struct midievent *me ) 
{
    printf(
        "[%06d] [%06d] %-15s c: %2d d1: %3d d2: %3d\n",
        (*me).timestamp.period,
        (*me).timestamp.frame,
        midievent_name_from_opcode(me),
        midievent_get_channel(me),
        (*me).data.lsb,
        (*me).data.msb );
}


int
midievent_cmp ( struct list_head *a, struct list_head *b )
{
  struct midievent *me_a = list_entry(a, struct midievent, list);  
  struct midievent *me_b = list_entry(b, struct midievent, list);

  if((*me_a).timestamp.period == (*me_b).timestamp.period) 
    return (*me_a).timestamp.frame > (*me_b).timestamp.frame; // returns 1 if bigger, 0 if smaller or equal
  else return (*me_a).timestamp.period > (*me_b).timestamp.period;
}


void
print_event_list ( struct list_head *el )
{
  struct list_head *pos;
  struct midievent *me;

  list_for_each( pos, el )
  {
    me = list_entry(pos, struct midievent, list);
    midievent_pretty_print(me);
  }
}
/*
int main( void )
{
  struct midievent melist;
  INIT_LIST_HEAD(&me_list.list);

  struct midievent *tmp;
  struct list_head *pos;
  struct list_head *q;

  uint i;
  for(i = 0; i < 1000000; i++)
  {
    tmp= (struct midievent *)malloc(sizeof(struct midievent));
    (*tmp).timestamp.period = rand() % 512; 
    (*tmp).timestamp.frame  = rand() % 512; 
    list_add(&((*tmp).list), &(me_list.list));
  }

  list_for_each(pos, &me_list.list)
  {
    tmp = list_entry(pos, struct midievent, list);
    //midievent_pretty_print(tmp);
  }

  list_sort(&me_list.list, midievent_cmp); 

  //printf("\n");
 
  list_for_each(pos, &me_list.list)
  {
    tmp = list_entry(pos, struct midievent, list);
    //midievent_pretty_print(tmp);
  }

  list_for_each_safe(pos, q, &me_list.list)
  {
    tmp= list_entry(pos, struct midievent, list);
    list_del(pos);
    free(tmp);
  }

  return 0;
}
*/
/*
struct test_list {
  list_head list;
  int num;
};

void
print_test_list ( struct list_head *el )
{
  struct list_head *pos;
  struct test_list *tl;

  list_for_each( pos, el )
  {
    tl = list_entry(pos, struct test_list, list);
    printf("num: %i\n", tl->num);
  }
}

int main( void )
{

  struct test_list list1;
  struct test_list list2;
  INIT_LIST_HEAD(&list1.list);
  INIT_LIST_HEAD(&list2.list);

  struct test_list *tmp;
  struct list_head *pos;
  struct list_head *q;

  for(uint i = 9; i != 0; --i)
  {
    tmp = (struct test_list*)malloc(sizeof(struct test_list));
    tmp->num = i;
    list_add(&tmp->list, &list2.list);
  }
  printf("LIST2\n");
  print_test_list(&list2.list);
  printf("LIST1\n");
  print_test_list(&list1.list);

  list_move_tail(list2.list.next, &list1.list);
  list_move_tail(list2.list.next, &list1.list);

  printf("LIST2\n");
  print_test_list(&list2.list);
  printf("LIST1\n");
  print_test_list(&list1.list);

  list_move(list1.list.next, &list2.list);
  list_move(list1.list.next, &list2.list);

  printf("LIST2\n");
  print_test_list(&list2.list);
  printf("LIST1\n");
  print_test_list(&list1.list);
}

*/

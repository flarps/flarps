/************************************************************************/
/* Copyright (C) 2009 Lieven Moors                                      */
/*                                                                      */
/* This file is part of flarps.                                         */
/*                                                                      */
/* Flarps is free software: you can redistribute it and/or modify       */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* Flarps is distributed in the hope that it will be useful,            */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        */
/* GNU General Public License for more details.                         */
/*                                                                      */
/* You should have received a copy of the GNU General Public License    */
/* along with Flarps.  If not, see <http://www.gnu.org/licenses/>.      */
/************************************************************************/

#ifndef ALL_H
#define ALL_H

#define elementsof(x) (sizeof((x)) / sizeof((x)[0]))

typedef unsigned char uchar ;
typedef signed char schar ;
typedef unsigned int uint ;
typedef signed int sint ;
typedef unsigned short ushort ;
typedef signed short sshort ;
typedef unsigned char byte_t ;

#endif

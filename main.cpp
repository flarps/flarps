/************************************************************************/
/* Copyright (C) 2009 Lieven Moors                                      */
/*                                                                      */
/* This file is part of flarps.                                         */
/*                                                                      */
/* Flarps is free software: you can redistribute it and/or modify       */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* Flarps is distributed in the hope that it will be useful,            */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        */
/* GNU General Public License for more details.                         */
/*                                                                      */
/* You should have received a copy of the GNU General Public License    */
/* along with Flarps.  If not, see <http://www.gnu.org/licenses/>.      */
/************************************************************************/

#include "main.h"


static const struct option long_option[] = {
        {"help", 0, NULL, 'h'},
};

static void usage(void)
{
  cout <<  "------------------------------------------------------------------------------------------" << endl;
  cout <<  "  Flarps is a MIDI arpeggiator for linux based systems.                                   " << endl;
  cout <<  "  Its operation is controlled by a text file, that can be edited while flarps is running. " << endl;
  cout <<  "  Flarps outputs MIDI events with JACK MIDI, and uses JACK transport for synchronization. " << endl;
  cout <<  "  For more information on how to use flarps, please read the README file that is          " << endl;
  cout <<  "  distributed with flarps.                                                                " << endl;
  cout <<  "                                                                                          " << endl;
  cout <<  "          Copyright (C) 2010 Lieven Moors                                                 " << endl;
  cout <<  "------------------------------------------------------------------------------------------" << endl;
  cout <<  "  Usage: flarps [-options] [file]                                                         " << endl;
  cout <<  "------------------------------------------------------------------------------------------" << endl;
  cout <<  "    -h,--help : print this information.                                                   " << endl;
  cout <<  "------------------------------------------------------------------------------------------" << endl;
}

void my_handler(int s){
  if(jack_client_close(CLIENT)) cerr << "Could not close JACK client!" << endl;
  else {
   cout << "\nBye Bye !" << endl;
   exit(1); 
  }
}

// global variables

map<string,ratio> RHYTHMS ;
float TEMPO = 60.0 ;
float BEAT = 0.25 ;
float SYNC = 1;
string filename = "" ;
jack_client_t* CLIENT ;
const uint N_PORTS = 8 ;

vector<jack_port_t*> PORTS;
void* PORT_BUF[N_PORTS];
// the minimum size of the memory pool used to allocate midi events
const uint FREE_LIST_SIZE = 1024;
// two lists: to get a new event, we take them from the free_list, 
// so we don't have to allocate from heap in process().
struct midievent event_list;
struct midievent event_free_list;

/* To store the note_on messages (one for every port), so they can be output in next cycle
   This gives us the ability to send note_off for the step in previous cycle,
   and allows us to keep track of time for each port (with period and frame members)
   Another plus: the first note send out can be a "zero" note, without
   changing the logic in jack.cpp
*/

struct midievent NOTES_ON[N_PORTS];

vector<Arp>* ARPS = new vector<Arp>() ;
vector<Arp>* ARPS_TMP = new vector<Arp>();

pthread_mutex_t MUTEX = PTHREAD_MUTEX_INITIALIZER; // my first mutex :-)

bool SWAP = false ;


int main (int argc, char **argv)
{

// Parsing options and arguments 

  int c;
	
  while ((c = getopt_long(argc, argv, "h", long_option, NULL)) != -1) 
  {
    switch (c){
      case 'h':
               usage();
               return 1;
      default:
               usage();
               return 1;
    }
  }

  if (argc == (optind + 1)) {
    filename = argv[optind];
  }
  else {
    cerr <<  "Usage: flarps [-options] [file]" << endl;
    return 1;
  }

// Catch SIGINT signal

  struct sigaction sig_int_handler;

  sig_int_handler.sa_handler = my_handler;
  sigemptyset(&sig_int_handler.sa_mask);
  sig_int_handler.sa_flags = 0;

  sigaction(SIGINT, &sig_int_handler, NULL);
  sigaction(SIGTERM, &sig_int_handler, NULL);

  struct midievent *tmp;

// allocate freelist

  INIT_LIST_HEAD(&event_list.list);
  INIT_LIST_HEAD(&event_free_list.list);

  for(uint i = FREE_LIST_SIZE; i > 0; --i)
  {
    tmp = (struct midievent *)malloc(sizeof(struct midievent));
    list_add(&tmp->list, &event_free_list.list);
  }

// -----------------------------------------

  pthread_t watch_thread ;

  initialize_rhythm_map() ;

	
  if( (CLIENT = jack_client_open ("FLARPS", (jack_options_t)0, NULL) ) == NULL)
  {
    cerr <<  "jack server not running?" << endl;
    return 1;
  }

  while(true)
  {
    try {
      read_load_file( ARPS ) ;
      break;
    }
    catch ( syntax_error& x ){
      cerr << "Syntax_error: " << x.what() << endl ;
      cerr << "Please correct the input file." << endl ;
      sleep(10);
    }
  }

  for( uint i = 0; i < N_PORTS; i++ )
  {
    stringstream str ;
    string port_name ;
    str << "flarp_" << i ;
    str >> port_name ;

    jack_port_t* port = jack_port_register( CLIENT, port_name.c_str(), JACK_DEFAULT_MIDI_TYPE, JackPortIsOutput, 0 ) ;
    PORTS.push_back( port ) ;
  }

  jack_set_process_callback( CLIENT, process, NULL);
  //jack_set_sync_callback( CLIENT, &sync, NULL ) ;


  if (jack_activate(CLIENT))
  { 
    cerr << "cannot activate client" << endl;
    return 1;
  }

  int w_ret = pthread_create( &watch_thread, NULL, watch_file, NULL ) ;
  if (w_ret != 0) cerr << "there was a problem creating the watch thread" << endl;

  pthread_join( watch_thread, NULL);

  delete ARPS ;
  delete ARPS_TMP ;
}


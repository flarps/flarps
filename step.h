/************************************************************************/
/* Copyright (C) 2009 Lieven Moors                                      */
/*                                                                      */
/* This file is part of flarps.                                         */
/*                                                                      */
/* Flarps is free software: you can redistribute it and/or modify       */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* Flarps is distributed in the hope that it will be useful,            */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        */
/* GNU General Public License for more details.                         */
/*                                                                      */
/* You should have received a copy of the GNU General Public License    */
/* along with Flarps.  If not, see <http://www.gnu.org/licenses/>.      */
/************************************************************************/

#ifndef STEP_H
#define STEP_H

#include <iostream>
#include <string>
#include "rhythm.h"
#include "common.h"

using namespace std ;

class Step {
	private:

	int height ;
	float width ;
	float duration ;
	uint velocity ;

	public:

	void set_height( int h ){ height = h ;} ;
	void set_width( string w ){ width = rhythm(w) ;} ;
	void set_duration( string d ){ duration = rhythm(d) ;} ;
	void set_velocity( uint v ){ velocity = v ;} ;	
	int get_height( ){ return height ;} ;
	float get_width( ){ return width ;} ;
	float get_duration( ){ return duration ;} ;
	uint get_velocity( ){ return velocity ;} ;
	
	Step( int h = 1, float w = 0.25, float d = 0.25, uint v = 127 ) ;
		
} ;

#endif

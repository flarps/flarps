.\" Process this file with
.\" groff -man -Tascii flarps.1
.\"
.TH FLARPS 1 "MARCH 1995" Linux "User Manuals"
.SH NAME
flarps \- an arpeggiator
.SH SYNOPSIS
.B flarps [-h]
.I file
.SH DESCRIPTION
.B flarps
is a MIDI arpeggiator for linux based systems.                                   
Its operation is controlled by a text file, which can be edited while flarps is running. 
Flarps outputs MIDI events with JACK MIDI, and uses JACK transport for synchronization. 
.SH OPTIONS
.IP "-h or --help"
Print a short help message
.SH USAGE
.PP
You can start flarps in a terminal, and it requires one argument.
This should be the pathname to an existing file you are going to use to interact with flarps. 
The file has to be syntactically correct at all times.
If this is not the case, flarps will refuse to (re)load the file,
and print an error message on your terminal window.
However, once a file has been loaded correctly,
flarps will keep running with the last known correct state.
Because of this, it might be easiest to start off with the example file "steps.txt",
which is distributed with the source code of flarps.
.PP
You interact with the program by modifying the text file in a text editor,
and every time you save it,
the program will automatically reload it. 
.PP  
Arpeggios are defined by a sequence of relative steps within a certain scale.
A step is about what happens between notes, both vertically (pitch), and horizontally (rhythm).
.PP
The height of a step determines the relative distance to the next note within a scale.
A step with a height of 2 for example will take you to E if you start from C in the C major scale.
It is important to understand that the height value is not expressed in half-notes.
A height value of 1 can represent one or two half-notes, depending on the current position in a scale.
Height values can be positive or negative integer values.
Positive values move upwards within the scale, negative values downwards.
.PP 
Besides its height, every step has a width and a duration.
Its width determines the distance between the start of the note (note-on) on the left side of the step,
and the start of the note on the right side of the step. 
The duration determines the end of the note (note-off) on the left side of the step.
.PP
.nf 
This can be visualized as such:
  
  ===              -> first note            ---
                                             |
  |      |         -> width of step          |
                                             |  -> height of step
  | |              -> duration of step       |
                                             |
         ======    -> second note           ---
.nf
.PP  
You can specify the widths and durations values with these letters:
.PP
.nf  
.BR "x " ": sixty-fourth"
.BR "t " ": thirty-second"
.BR "s " ": sixteenth"
.BR "e " ": eighth"
.BR "q " ": quarter"
.BR "h " ": half"
.BR "w " ": whole"
.PP
.fi
In addition you can prefix these letters with a 't',
if you want a triple note value,
and you can use one or more dots '.' as a postfix if you want a dotted note.
.PP
NOTE: this representation of rhythmical patterns has been taken from Common Music <http://commonmusic.sourceforge.net/>, written by Heinrich Taube.
.PP  
Flarps uses JACK MIDI to generate midi messages for upto eight arpeggios you can specify in the file.
When you start up flarps, you will get eight midi output ports,
one for every possible arp,
which you can connect to external midi software.
.PP   
NOTE: This is currently hard-coded.
.PP  
Jack transport should be rolling (you can press the play button in qjackctl to start it),
for the arpeggios to play.
.SH THE TEXT FILE
.PP
Let me give a simple example of a possible text file first,
and then explain it line by line.
.PP
.nf  
--------------------------------
tempo 120.0
beat 0.25
sync q
  
0 flarps 2 # scale with two sharps
0 height          2  2  -1 3 -4
0 width           e  s  s
0 duration        s. e  t
0 velocity        100 110 108
0 octave 4
0 index 2
0 min -4
0 max 4
0 mode 0
0 channel 0
--------------------------------
.fi
.SH GLOBAL PARAMETERS
.PP
This file only specifies one arpeggio.
The 'tempo','beat' and 'sync' values are global values,
which means that they have an influence on all arpeggios that are specified in this file.
.TP
.B tempo, beat:
The 'tempo' and 'beat' values determine the speed with which the notes are played.
The 'beat' value determines what value is considered as one beat in a specific tempo. 
A value of 0.25 for example would make a quarter note ('q') the beat unit.
If the tempo is set to 120 beats per minute,
the program would play two quarter notes per second.
.TP
.B sync:
Whenever you save the file, all the parameters in the file will be loaded by the program.
The 'sync' value lets you control at what time the program will switch to those new parameters.
You can use any of the rhythm symbols (see above) as a sync value.
For example: when you set sync to 'e', and you write out the file,
flarps will calculate on an absolute timeline (starting from time 0),
when the next eighth note would be due.
When that time comes,
flarps will switch to the new definitions in the file.

NOTE: time is only running when jack's transport is rolling.
.SH ARPEGGIO PARAMETERS
.PP
All the following lines in the example file start with the number 0,
and this means that these lines belong to the first arpeggio,
which will be generated on the first midi port in JACK.
The number you use is not that important,
but it cannot be negative.
All lines that start with the same number will be used to configure one arpeggio,
and the smallest number will be sent on the first port.
.PP 
You don't have to specify all lines to start an arpeggio,
but you should have at least one, if you want an arpeggio to play.
The configuration lines you leave out, will be set to default values.
.TP
.B flarps:
With 'flarps' you can indicate how many flats or sharps you want in your scale.
Use a negative number to indicate a number of flats, and a positive number for sharps.
It is possible to specify bigger (or smaller) numbers than you would normally expect.
For example if you set the value to 10,
the program will transpose to 10 sharps, starting from C major.
The notes that were already augmented will be augmented twice.
.TP
.B octave, index:
With 'octave', you specify in which octave you want the arpeggio to start.
You can specify integer values between 0 and 10. 

With the value of 'index' you can specify on which note within one octave you want the arpeggiator to start.
The index value can be a value between 0 and 6.
If you don't use more than 7 sharps or 7 flats in your scale (see the 'flarps parameter'),
a value of zero means that the arpeggio will start on a C, a C#, or a Cb,
depending on the number of flats/sharps.

Let me explain how this works.
In order to transpose to different scales,
flarps keeps track of something called the 'basenote'.
When you have zero flats or sharps, the basenote will be a C.
With 'octave' set to 4, and 'index' to 2,
the note from where the arpeggio starts will be E4.
If you would use a scale with two sharps (value of parameter 'flarps' is 2),
the basenote would become C#.
This is necessary because of the transposition, which augments C.
With 'octave' set to 4, and 'index' to 2,
The first note would still be E4,
but with the 'index' set to 0,
the first note of the arpeggio would be the same as the basenote,
which is C#4.
.TP
.B height, width, duration, velocity:
With 'height', 'width', 'duration' and 'velocity',
you can specify the steps as explained in a previous section,
and with which velocities the notes are played.
These lines should be grouped together in "steps.txt",
although their order is not important.
If these lists have a different lengths,
the shorter lists will be repeated (looped) from the start
until all lists have an equal length for the arpeggiator.
The 'velocity' parameter has a possible range between 0 and 127.

As an additional feature,
it is possible to group these values between parenthesis,
and specify how many times this pattern should be repeated.
You could have a line like this for example:
.nf

0 height (2 3 -1)2 -5

.nf
The '2' that comes immediatly after the right parenthesis indicates
how many times the pattern between the parenthesis should be repeated.
This line would expand to: 2 3 -1 2 3 -1 -5.
The same can be done with widths, durations and velocities.
It is currently not possible to nest these expressions.
.TP
.B min, max:
With 'min' and 'max', you can specify two limits which will confine the arpeggiator.
The values of these limits are also relative step sizes,
and are relative to the first note of the arpeggio.
If you start at C4 for example a 'min' value of -4,
and a 'max' value of 4 will confine the arpeggio to F3 and G4.
.TP
.B mode:
With 'mode' you can determine what happens when the arpeggio reaches the 'min' and 'max' boundaries.
Currently, there are two modes available: 0 and 1.
If the value of 'mode' is 0, the arpeggio will wrap-around at the specified boudaries.
So when a step exceeds the 'max' value, 
the part of the step that crosses the threshold will be added to the 'min' value. 
If the 'mode' value is 1,
the step that would cross a 'min' or 'max' boundary is mirrored against that boundary,
and the sign of all the steps are reversed when that happens.
NOTE: in future versions of the program intend to add more useful modes to the program.
.TP
.B channel:
With 'channel' you can choose on which midi channel the arpeggio will play.
.SH KNOWN LIMITATIONS AND BUGS:
.IP \(bu 4
A number of hardcoded values that have to be turned into command line arguments.
.IP \(bu 4
Flarps cannot act as a timbase master yet, so it cannot set the tempo of JACK transport.
.IP \(bu 4
Synchronization with other programs should theoretically be possible, 
if the tempo specified in "steps.txt" matches the tempo of an external program,
but I have not tested this yet.
.IP \(bu 4
If MIN and MAX are set to the same value, the program will be aborted with a floating
point exception.
.SH AUTHOR
Lieven Moors
.SH COPYRIGHT
.PP
Flarps Copyright (c) 2010 Lieven Moors.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it. 
There is NO WARRANTY, to the extent permitted by law.

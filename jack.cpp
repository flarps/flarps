/************************************************************************/
/* Copyright (C) 2009 Lieven Moors                                      */
/*                                                                      */
/* This file is part of flarps.                                         */
/*                                                                      */
/* Flarps is free software: you can redistribute it and/or modify       */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* Flarps is distributed in the hope that it will be useful,            */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        */
/* GNU General Public License for more details.                         */
/*                                                                      */
/* You should have received a copy of the GNU General Public License    */
/* along with Flarps.  If not, see <http://www.gnu.org/licenses/>.      */
/************************************************************************/

#include "jack.h"

using namespace std ;

void flush_notes_off( jack_nframes_t nframes )
{
  struct midievent *me;
  struct list_head *pos, *q;
  list_for_each_safe(pos, q, &event_list.list)
  {
    me = list_entry(pos, struct midievent, list);
    uchar* event_buf = jack_midi_event_reserve(PORT_BUF[me->port], 0, 3);
    if(event_buf) midievent_get_raw(me, event_buf, 3); 
    else fprintf(stderr, "error: jack_midi_event_reserve() failed\n");
    list_move(event_list.list.next, &event_free_list.list);
  }
}

// Calculate a new timestamp (period, frame) based on a previous one,
// and a length in seconds.

struct timestamp 
calculate_new_time( struct timestamp start_time, float seconds, 
         jack_nframes_t sample_rate, jack_nframes_t period_size )
{
  struct timestamp ts;

  jack_nframes_t frames = round(seconds * sample_rate); // Seconds to frames.
  jack_nframes_t frames_rel = start_time.frame + frames; // Frames after adding seconds to start_time.
  jack_nframes_t period_rel =  floor(frames_rel / period_size); // How many periods fit in those frames.

  ts.frame = fmod(frames_rel, period_size); // How many frames are left after removing all full periods.
  ts.period =  start_time.period + period_rel; // Add the periods to periods of start_time.
  return ts;
}

// Find the next possible sync time expressed in periods.
// Argument 'sync' is the sync unit that can be defined in the text file.
// We see how many of these sync units fit in the time before period_current,
// and use the one after that (which is in the future) as the time to sync.

ulong calculate_sync_time( float sync, jack_nframes_t period_current,
                             jack_nframes_t sample_rate, jack_nframes_t period_size  )
{
  float period_secs = static_cast<float>(period_size) / sample_rate; // The size of one period in seconds.
  float period_current_secs = period_secs * period_current; // The time of period_current in seconds
  long  sync_time_in_syncs = floor(period_current_secs / sync) + 1; // See how many times SYNC fits in this length, +1 is when we are going to sync.
  float sync_time_in_secs = sync_time_in_syncs * sync; // Translate our sync_time to seconds.
  return floor(sync_time_in_secs / period_secs); // Translate our sync_time to periods.
  //printf("sync period = %li current period = %i\n", sync_period, period_current);
}


// Take an event from event_free_list, copy another midievent into it,
// and move it to the event_list.

void event_to_event_list ( struct midievent *me )
{
  if(!list_empty(&event_free_list.list))
  {
    struct midievent *tmp = list_entry(event_free_list.list.next, struct midievent, list);
    midievent_copy( tmp, me );
    list_move_tail(event_free_list.list.next, &event_list.list);
  }
  else printf("error: not enough events in events_free_list!");
}

// Sum all step_widths

float sum_steps_widths( vector<Step>* steps  )
{
  float sum = 0;

  for(uint i = 0; i < steps->size(); i++){
    sum += (*steps)[i].get_width(); 
  }
  return sum;
}

// This function is needed when new arps are added while flarps is running,
// or when jack transport changes position.
// We calculate the timestamp that would come just before period_current.
// First we use the sum of all steps as a unit to move forwards,
// and we stop before crossing period_current.
// Then we move on in individual steps, and stop before we crossed period_current.

struct timestamp 
update_time( vector<Step> *steps, jack_nframes_t period_current, 
                            jack_nframes_t sample_rate, jack_nframes_t period_size  )
{
  struct timestamp ts1 = {0, 0};
  uint i = 0;

  float length_secs = sum_steps_widths(steps); // Get length of all steps combined in seconds.
  float period_current_secs = (static_cast<float>(period_size) / sample_rate) * period_current; // Period_current in seconds
  float n = floor(period_current_secs / length_secs); // How many times length of all steps combined fit before period_current
  float secs = n * length_secs; // How long this is in seconds.

  ts1 = calculate_new_time(ts1, secs, sample_rate, period_size);

  while(ts1.period < period_current)
  {
    ts1 = calculate_new_time( ts1, (*steps)[i].get_width(), sample_rate, period_size );
    i++; if(i == steps->size()) i = 0;
  }
  //cout << "period: " << ts1.period << "frame " << ts1.frame << endl;
  return ts1;
}

// JACK PROCESS CALLBACK

int process(jack_nframes_t nframes, void *arg)
{
  jack_transport_state_t j_state ;
  jack_position_t j_position ;
  j_state = jack_transport_query( CLIENT, &j_position ) ;

  jack_nframes_t sample_rate = j_position.frame_rate ;
  jack_nframes_t start_frame = j_position.frame ;
  jack_nframes_t period_size = nframes ;

  static jack_nframes_t period_current = 0;
  static jack_nframes_t period_previous = 0;

  static ulong sync_period = 0;
  static int arps_size_diff = 0;

  static bool first_cycle = true;
  static bool position_changed = false;
  static bool stopped_once = false;
  static bool init_notes_on = true;
  static bool calculate_sync = true;

  // Calculate current period from start_frame.
  period_current = start_frame / period_size;

  // Get buffers for each arp.
  // jack docs: don't use these pointers accross multiple process calls !
  for(uint i = 0; i < N_PORTS; i++)
  {
    PORT_BUF[i] = jack_port_get_buffer(PORTS[i], nframes); 
    if(PORT_BUF[i]) jack_midi_clear_buffer(PORT_BUF[i]); 
    else fprintf(stderr, "error: jack_port_get_buffer() failed\n");
  }
  // When a new file is loaded (SWAP is true), and when sync_period has arrived, we swap the pointers ARPS and ARPS_TMP.
  if(pthread_mutex_trylock(&MUTEX) == 0) // TODO: don't repeat lock when we have calculated sync period, only lock when period_current == sync_period
  {
    if(SWAP == true)
    {
      if(calculate_sync) // TODO: this should be first check, only if calculate_sync is true we should lock and check SWAP.
      {
        sync_period = calculate_sync_time( SYNC, period_current ,sample_rate, period_size);
        calculate_sync = false;
      }

      if(sync_period == period_current) // TODO: don't do this within lock!
      {
        flush_notes_off( nframes ) ;

        arps_size_diff = ARPS_TMP->size() - ARPS->size();

        vector<Arp>* arps = ARPS; //swapping arps
        ARPS = ARPS_TMP;
        ARPS_TMP = arps;

        SWAP = false;
        calculate_sync = true;
        init_notes_on = true;
      }
    }
    pthread_mutex_unlock(&MUTEX);
  }
  // When new arps are added while flarps is running, we have to make sure that a NOTE_ON is initialised.
  if(arps_size_diff > 0)
  {
    uint index = 0;

    for(uint i = arps_size_diff; i > 0; i--)
    {
      index = ARPS->size() - i;
      midievent_set_opcode(&NOTES_ON[index], NOTE_ON);
      midievent_set_channel(&NOTES_ON[index], (*ARPS)[index].channel);
      midievent_set_note(&NOTES_ON[index], (*ARPS)[index].scale.get_step());
      midievent_set_note_velocity(&NOTES_ON[index], 127);
      NOTES_ON[index].port = index;
      NOTES_ON[index].timestamp = update_time(&(*ARPS)[index].steps, period_current, sample_rate, period_size);
    }
    arps_size_diff = 0;
  }

  if( j_state == JackTransportRolling )
  {
    stopped_once = false ;

    if((first_cycle == false) && (period_current != (period_previous + 1))){
      position_changed = true;
      //printf("position changed.\n period_previous = %i | period_current = %i\n", period_previous, period_current);
    }
    period_previous = period_current;
    if(position_changed) flush_notes_off(nframes);

    for( uint i = 0; i < ARPS->size(); i++ )
    {
      Arp* arp = &(*ARPS)[i] ;
      vector<Step>* steps = &arp->steps ;
      Scale* scale = &arp->scale ;
      byte_t j = arp->index_steps ;

      if(init_notes_on)
      {
        midievent_set_opcode(&NOTES_ON[i], NOTE_ON);
        midievent_set_channel(&NOTES_ON[i], arp->channel); // set channel
        midievent_set_note(&NOTES_ON[i], scale->get_step()); // get a first note_on to get started
        midievent_set_note_velocity(&NOTES_ON[i], 127); // set velocity, otherwise it will be interpreted as note_off
        NOTES_ON[i].port = i; // cout << "arp " << i << endl;
      }

      if(position_changed)
      {
        NOTES_ON[i].timestamp = update_time(steps, period_current, sample_rate, period_size);
      }

      while(true) // Loop through steps for this arp, until next_on is not part of this period anymore.
      {
        Step* step = &(*steps)[j] ;
        // Get a pointer to next note_on event. This should be done in loop, because the loop will change the event.
        struct midievent *note_on = &NOTES_ON[i];

        if(note_on->timestamp.period == period_current) // See if note_on is part of this period.
        {
          event_to_event_list(note_on);

          struct midievent note_off = *note_on; // Make a copy and change opcode.
          midievent_set_opcode(&note_off, NOTE_OFF);

          // Calculate timestamp for the corresponding note_off event
          struct timestamp time = calculate_new_time(note_on->timestamp, step->get_duration(), sample_rate, period_size);
          note_off.timestamp = time;
          //note_off.pretty_print();
          event_to_event_list(&note_off);

          // Calculate timestamp for the note_on event.
          time = calculate_new_time(note_on->timestamp, step->get_width(), sample_rate, period_size);

          note_on->timestamp = time;
          note_on->port = i;

          midievent_set_opcode(note_on, NOTE_ON);
          midievent_set_channel(note_on, arp->channel);
          midievent_set_note (note_on, scale->take_step(step->get_height()));
          midievent_set_note_velocity(note_on, step->get_velocity());
        }
        else break;

        ++j; if(j == arp->steps.size()) j = 0;
      }

      // store state of the arp
      arp->index_steps = j;
    }

    position_changed = false;

    // Sort event_list with merge sort.
    list_sort(&event_list.list, midievent_cmp);

    // write out events for this period to JACK.
    struct list_head *pos;
    struct list_head *q;
    struct midievent *me;

    list_for_each_safe(pos, q, &event_list.list)
    {
      me = list_entry(pos, struct midievent, list);
      if(me->timestamp.period <= period_current)
      {
        uchar* event_buf = jack_midi_event_reserve(PORT_BUF[me->port], me->timestamp.frame, 3);
        if(event_buf) midievent_get_raw(me, event_buf, 3); 
        else fprintf(stderr, "error: jack_midi_event_reserve() failed\n");
        list_move(event_list.list.next, &event_free_list.list);
      }
      else break;
    }

    // Disable the initialization of NOTE_ON[i] after a first run through the arps.
    init_notes_on = false;
  }

  else if ( (j_state == JackTransportStopped && !stopped_once) )
  {
     flush_notes_off( nframes ) ;
     stopped_once = true ;
  }

  if(first_cycle == true)
    first_cycle = false;

  return 0;
}

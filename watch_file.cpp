/************************************************************************/
/* Copyright (C) 2009 Lieven Moors                                      */
/*                                                                      */
/* This file is part of flarps.                                         */
/*                                                                      */
/* Flarps is free software: you can redistribute it and/or modify       */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* Flarps is distributed in the hope that it will be useful,            */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        */
/* GNU General Public License for more details.                         */
/*                                                                      */
/* You should have received a copy of the GNU General Public License    */
/* along with Flarps.  If not, see <http://www.gnu.org/licenses/>.      */
/************************************************************************/

#include "watch_file.h"

using namespace std ;



void* watch_file( void* ptr )
{
  int fd = inotify_init() ;

  if ( fd < 0 ){ 
    perror( "inotify_init" ) ; 
  }
  

  while(true)
  {
    int wd = inotify_add_watch( fd, filename.c_str(), IN_MODIFY) ;

    if ( wd < 0 ){
      perror( "inotify_add_watch" ) ;
    }

    char buf[BUF_LEN] ;
    int i = 0 ;
    int len = read( fd, buf, BUF_LEN ) ;;

    if ( len < 0 ) {
      if ( errno == EINTR ){
      }
      else {
        perror ( "read" );
      }
    } 
    else if (!len){
      cout << "BUF_LEN too small?" << endl ;
    }

    while (i < len) 
    {
      struct inotify_event *event;
      event = (struct inotify_event *) &buf[i];

      if ( event->wd == wd )
      {
        if(pthread_mutex_lock(&MUTEX) == 0)
        {
          ARPS_TMP->clear() ;

          try {
            read_load_file( ARPS_TMP ) ;
            SWAP = true;
          }
          catch ( syntax_error& x ){
            cout << "Syntax_error: " << x.what() << endl ;
            cout << "Please correct the input file." << endl ;
          }

          pthread_mutex_unlock(&MUTEX);
        }
        else fprintf(stderr, "could not load file because mutex was locked");
      }

      i += EVENT_SIZE + event->len;
    }
  }
pthread_exit(NULL);
}


/************************************************************************/
/* Copyright (C) 2009 Lieven Moors                                      */
/*                                                                      */
/* This file is part of flarps.                                         */
/*                                                                      */
/* Flarps is free software: you can redistribute it and/or modify       */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* Flarps is distributed in the hope that it will be useful,            */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        */
/* GNU General Public License for more details.                         */
/*                                                                      */
/* You should have received a copy of the GNU General Public License    */
/* along with Flarps.  If not, see <http://www.gnu.org/licenses/>.      */
/************************************************************************/

#ifndef FILE_H
#define FILE_H

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <stdexcept>
#include <boost/rational.hpp>
#include <stdio.h>
#include <stdlib.h>
#include <regex.h>
#include "common.h"
#include "arp.h"
#include "rhythm.h" // used to load SYNC (with rhythm)

using namespace std ;
using boost::rational ;

typedef rational<int> ratio ;
typedef vector<string> vec_str ;
typedef vector< vector<string> > vec_str_2 ;

typedef pair< string, bool (*)(const vec_str&, uint)> pair_sf ;
typedef map< string, bool (*)(const vec_str&, uint)> property_map ;
typedef map< string, bool (*)(const vec_str&, uint)>::iterator property_map_i ;

extern map<string,ratio> RHYTHMS ;
extern float TEMPO ;
extern float BEAT ;
extern float SYNC ;
extern string filename ;

class file_error : public runtime_error {
	public:
	file_error(const string& msg = "") : runtime_error(msg) {} ;
} ;

class syntax_error : public runtime_error {
	public:
	syntax_error(const string& msg = "") : runtime_error(msg) {} ;
} ;

void read_load_file( vector<Arp>* arps ) ;
void print_arps( vector<Arp>* arps ) ; // only used as forward declaration (want this function near end of file)
void expand_repeats(vector<string>& lines) ; // same here



#endif

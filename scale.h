/************************************************************************/
/* Copyright (C) 2009 Lieven Moors                                      */
/*                                                                      */
/* This file is part of flarps.                                         */
/*                                                                      */
/* Flarps is free software: you can redistribute it and/or modify       */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* Flarps is distributed in the hope that it will be useful,            */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        */
/* GNU General Public License for more details.                         */
/*                                                                      */
/* You should have received a copy of the GNU General Public License    */
/* along with Flarps.  If not, see <http://www.gnu.org/licenses/>.      */
/************************************************************************/

#ifndef SCALE_H
#define SCALE_H


class Scale {

private:

	int SCALE[7] ;
	int FLARPS ;
	int BASENOTE ;
	int INDEX ;
	int MIN ;
	int MAX ;
	int MODE ;
	int STEPS_SUM ;
	int SIGN ;

	void init_scale () ;
	void transpose_scale ( int index ) ;
	int flarps_to_basenote_inc ( int flarps ) ;
	void transpose_basenote ( int flarps ) ;
	int flarps_to_index ( int flarps ) ;
	int stepsize_to_halfnotes ( int stepsize ) ;
	int stepsize_to_hfb ( int stepsize ) ;
	int hfb_to_basenote_inc( int hfb ) ;
	int stepsize_to_index_inc( int stepsize ) ;
	int fit( int num ) ;

public:

	Scale();

	void set_flarps( int f ){ transpose( f ) ;} ;
	void set_index( int i ){ INDEX = i ;} ;
	void set_octave( int o ){ BASENOTE = (BASENOTE % 12) + (12 * o) ;} ;
	void set_mode( int m ){ MODE = m ;} ;
	void set_min( int m ){ MIN = m ;} ;
	void set_max( int m ){ MAX = m ;} ;
	int get_flarps(){ return FLARPS ;} ;
	int get_index(){ return INDEX ;} ;
	int get_octave(){ return BASENOTE / 12 ;} ;
	int get_mode(){ return MODE ;} ;
	int get_min(){ return MIN ;} ;
	int get_max(){ return MAX ;} ;

	void transpose ( int flarps ) ;
	int take_step ( int stepsize ) ;
	int get_step () ;
	void debug() ;

};

#endif

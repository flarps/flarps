/************************************************************************/
/* Copyright (C) 2009 Lieven Moors                                      */
/*                                                                      */
/* This file is part of flarps.                                         */
/*                                                                      */
/* Flarps is free software: you can redistribute it and/or modify       */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* Flarps is distributed in the hope that it will be useful,            */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        */
/* GNU General Public License for more details.                         */
/*                                                                      */
/* You should have received a copy of the GNU General Public License    */
/* along with Flarps.  If not, see <http://www.gnu.org/licenses/>.      */
/************************************************************************/

#ifndef JACK_H
#define JACK_H

#include <set>
#include <math.h>
#include <jack/jack.h>
#include <jack/midiport.h>
#include <jack/transport.h>
#include "common.h"
#include "arp.h"
#include "midi.h"
#include "list_sort.h"

extern jack_client_t* CLIENT ;

extern const uint N_PORTS ;
extern vector<jack_port_t*> PORTS ;
extern void* PORT_BUF[];

extern struct midievent event_list;
extern struct midievent event_free_list;
extern struct midievent NOTES_ON[];

extern vector<Arp>* ARPS ;
extern vector<Arp>* ARPS_TMP ;
extern pthread_mutex_t MUTEX ;
extern bool SWAP;
extern float SYNC;

//variables used in process()

int process( jack_nframes_t nframes, void *arg ) ; 

extern bool STOPPED_ONCE ;

#endif
